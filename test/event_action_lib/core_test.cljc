(ns event-action-lib.core-test
  (:require [clojure.test :refer :all]
            [event-action-lib.core :as eal]
            [eftest.runner :as eftest]))

(defn gen-test-id []
  (keyword (gensym (testing-contexts-str))))

(deftest dispatch!
  (testing "Should throw an error when action does not exist."
    (let [event-id (gen-test-id)]
      (eal/reg-event! {:id event-id
                       :handler-fn (fn [a] {:nonexistant-action a})})
      (is (thrown? clojure.lang.ExceptionInfo (eal/dispatch! [event-id 1])))))

  (testing "Should throw an error when event does not exist."
    (is (thrown? clojure.lang.ExceptionInfo (eal/dispatch! [:nonexistant-event 1]))))

  (testing "Should throw an error when input does not exist."
    (let [event-id (gen-test-id)
          action-id (gen-test-id)]
      (eal/reg-action! {:id action-id
                        :execute-fn #(is false)})
      (eal/reg-event! {:id event-id
                       :inputs [:nonexistant-input]
                       :handler-fn (fn [a] {:action-id a})})

      (is (thrown? clojure.lang.ExceptionInfo (eal/dispatch! [event-id 1]))))))

(deftest eval-event-and-inputs!
  (testing "Should return actions."
    (let [event-id (gen-test-id)
          action-id (gen-test-id)
          test-data {:test 1}]
      (eal/reg-action! {:id action-id
                        :execute-fn #(is false)})
      (eal/reg-event! {:id event-id
                       :handler-fn (fn [{:keys [data]}] {action-id data})})
      (is (= {action-id test-data}
             (eal/eval-event-and-inputs! [event-id test-data])))))

  (testing "Should not execute actions."
    (let [event-id (gen-test-id)
          action-id (gen-test-id)
          test-data {:test 1}]
      (eal/reg-action! {:id action-id
                        :execute-fn #(is false)})
      (eal/reg-event! {:id event-id
                       :handler-fn (fn [{:keys [data]}] {action-id data})})
      (eal/eval-event-and-inputs! [event-id test-data])))

  (testing "Should evaluate inputs."
    (let [event-id (gen-test-id)
          input-id (gen-test-id)
          action-id (gen-test-id)
          test-input-result {:test 1}]
      (eal/reg-input! {:id input-id
                       :eval-fn (fn [data] test-input-result)})
      (eal/reg-event! {:id event-id
                       :inputs [input-id]
                       :handler-fn
                       (fn [{:keys [data inputs]}]
                         {action-id inputs})})
      (is (= {action-id {input-id test-input-result}}
             (eal/eval-event-and-inputs! [event-id {:test 1}]))))))

(deftest dispatch-action
  (testing "Should exist by default without declaring it"
    (let [event-id1 (gen-test-id)
          event-id2 (gen-test-id)
          test-data 1]
      (eal/reg-event! {:id event-id1
                       :handler-fn
                       (fn [{:keys [data]}]
                         {:dispatch! [event-id2 data]})})
      (eal/reg-event! {:id event-id2
                       :handler-fn
                       (fn [{:keys [data]}]
                         (is (= data test-data))
                         [])})
      (eal/dispatch! [event-id1 test-data]))))

(eftest/run-tests (eftest/find-tests "test"))

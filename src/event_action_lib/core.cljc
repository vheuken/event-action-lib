(ns event-action-lib.core)

(def ^:private event-registry (atom {}))
(def ^:private action-registry (atom {}))
(def ^:private input-registry (atom {}))

(defn reg-input! [{:keys [id] :as input}]
  (swap! input-registry assoc id input))

(defn reg-event! [{:keys [id] :as event}]
  (swap! event-registry assoc id event))

(defn reg-action! [{:keys [id] :as action}]
  (swap! action-registry assoc id action))

(defn- execute-actions! [actions]
  (doseq [[id data] actions]
    (if-let [execute-fn (:execute-fn (id @action-registry))]
      (execute-fn data)
      (throw (ex-info "No execute-fn registered for action." {:causes #{id}})))))

(defn- eval-inputs! [inputs data]
  (->> inputs
       (map (fn [id]
              (if-let [eval-fn (:eval-fn (id @input-registry))]
                [id (eval-fn data)]
                (throw (ex-info "No eval-fn registered for input." {:causes #{id}})))))
       (into {})))

(defn dispatch!
  "Evaluates an event, its inputs, and its actions.
  Takes an `event` vector where the first element is the `event-id` and the
  second element is the `event-data`"
  [[event-id event-data]]
  (let [{:keys [handler-fn inputs]} (get @event-registry event-id)]
    (if handler-fn
      (let [actions (handler-fn {:data event-data
                                 :inputs (eval-inputs! inputs event-data)})]
        (execute-actions! actions))
      (throw (ex-info "No handler-fn registered for event." {:causes #{event-id}})))))

(defn eval-event-and-inputs!
  "Evaluates an event and its inputs without performing its actions.
  Takes an `event` vector where the first element is the `event-id` and the
  second element is the `event-data`"
  [[event-id event-data]]
  (let [{:keys [handler-fn inputs]} (event-id @event-registry)]
    (handler-fn {:data event-data
                 :inputs (eval-inputs! inputs event-data)})))

(reg-action! {:id :dispatch!
              :execute-fn
              (fn [data]
                (dispatch! data))})

;;;; Example:
(comment
  (reg-action! {:id :test-action
                :execute-fn (fn [data] (println data))})

  (reg-event! {:id :foo
               :handler-fn (fn [{:keys [data]}] {:this-action-isnt-registered data
                                                 :test-action "But this one is!"})})

  (reg-action! {:id :print
                :execute-fn #(println %1)})

  (reg-event! {:id :add
               :handler-fn (fn [{:keys [data]}]
                             {:print (+ (first data) (second data))})})
  (dispatch! [:foo :bar])
  (dispatch! [:add [1 2]])

  ;;;; Input example
  (reg-input! {:id :some-external-source
               :eval-fn (fn [data]
                          (str "This is from an external source!\n"
                               "Maybe a database. Maybe HTTP. Maybe a file. Could be anything!\n"
                               "It has access to the data passed into the event: " data))})

  (reg-event! {:id :test-event-with-input
               :inputs [:some-external-source]
               :handler-fn (fn [{:keys [data inputs]}]
                             {:print inputs})})

  (dispatch! [:test-event-with-input :test-data]))

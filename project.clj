(defproject event-action-lib "0.0.3"
  :description "An event handling library"
  :url "rateless.net"
  :license {:name "zlib License"
            :url "http://zlib.net/zlib_license.html"}
  :deploy-repositories [["releases" :clojars]
                        ["snapshots" :clojars]]
  :dependencies [[org.clojure/clojure "1.10.1-beta2"]
                 [org.clojure/tools.namespace "0.2.11"]
                 [proto-repl "0.3.1"]
                 [eftest "0.5.7"]]
  :plugins [[lein-codox "0.10.6"]
            [lein-eftest "0.5.7"]])

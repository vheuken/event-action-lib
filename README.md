# event-action-lib

[![pipeline status](https://gitlab.com/vheuken/event-action-lib/badges/master/pipeline.svg)](https://gitlab.com/vheuken/event-action-lib/commits/master)


A Clojure library for flow-driven development and handling events

## Usage

FIXME

## License

Copyright © 2019 Vincent Heuken

Distributed under the zlib license.
